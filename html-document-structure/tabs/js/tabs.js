const tabs = document.querySelector('.tabs-nav');
const tabsDemo = tabs.firstElementChild;

const articles = document.querySelectorAll('.tabs-content [data-tab-icon]');

Array.from(articles).forEach((article) => {
  var newTab = tabsDemo.cloneNode(true);
  newTab.firstElementChild.textContent = article.dataset.tabTitle;
  newTab.firstElementChild.classList.add(article.dataset.tabIcon);

  article.classList.add('hidden');
  newTab.addEventListener('click', (e) => {
    var activeTab = document.querySelector('.ui-tabs-active');
    activeTab.classList.remove('ui-tabs-active');
    e.target.parentElement.classList.add('ui-tabs-active');    

    Array.from(articles).filter( _article => {
      return !_article.classList.contains('hidden');
    })[0].classList.add('hidden');

    article.classList.remove('hidden');
    });
  tabs.appendChild(newTab);
})

tabsDemo.remove();

articles[0].classList.remove('hidden');
tabs.firstElementChild.classList.add('ui-tabs-active');