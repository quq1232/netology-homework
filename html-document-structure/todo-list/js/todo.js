const done = document.querySelector('.done');
const undone = document.querySelector('.undone');

document.querySelector('.todo-list').addEventListener('change', (e) => {
  var node = e.target.parentElement;
  if( e.target.checked ) {
    undone.removeChild(node);
    done.appendChild(node);
  } else {
    done.removeChild(node);
    undone.appendChild(node);    
  }
})