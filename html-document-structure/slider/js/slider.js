const prev = document.querySelector('[data-action="prev"]');
const next = document.querySelector('[data-action="next"]');
const first = document.querySelector('[data-action="first"]');
const last = document.querySelector('[data-action="last"]');

const navigation = document.querySelector('.slider-nav');
const slideList = document.querySelector('.slides');

let activeElement = slideList.firstElementChild;

function init() {
  activeElement.classList.add('slide-current');
  prev.classList.add('disabled');
  first.classList.add('disabled');
}

document.addEventListener('DOMContentLoaded', init);

navigation.addEventListener('click', (event) => {
  activeElement = document.querySelector('.slide-current');
  activeElement.classList.remove('slide-current');
  switch (event.target) {
    case prev:
      if( prev.classList.contains('disabled') ) {
        break;
      }
      activeElement = activeElement.previousElementSibling;
      break;
    case next:
      if( next.classList.contains('disabled') ) {
        break;
      }
      activeElement = activeElement.nextElementSibling;
      break;
    case first:
      if( first.classList.contains('disabled') ) {
        break;
      }
      activeElement = activeElement.parentElement.firstElementChild;
      break;
    case last:
      if( last.classList.contains('disabled') ) {
        break;
      }
      activeElement = activeElement.parentElement.lastElementChild;
      break;
  }

  activeElement.classList.add('slide-current');

  if( activeElement.previousElementSibling ) {
    prev.classList.remove('disabled');
    first.classList.remove('disabled');
  } else {
    prev.classList.add('disabled');
    first.classList.add('disabled');
  }

  if( activeElement.nextElementSibling ) {
    next.classList.remove('disabled');
    last.classList.remove('disabled');
  } else {
    next.classList.add('disabled');
    last.classList.add('disabled');
  }
});