const bookSource = "https://netology-fbb-store-api.herokuapp.com/book/";
const ul = document.querySelector("#content");

/*
 * const listPart1 = `<li data-title=\"`;
 * const listPart2 = `\" data-author=\"`;
 * const listPart3 = `\" data-info=\"`;
 * const listPart4 = `\" data-price=\"`;
 * const listPart5 = `\"><img src=\"`;
 * const listPart6 = `\"></li>`;
*/
var xhr = new XMLHttpRequest();
xhr.open("GET", bookSource);
xhr.addEventListener("load", onLoad);
xhr.send();

function onLoad() {
  if (xhr.status == 200) {
    var booksList = JSON.parse(xhr.responseText).reduce(function(list, book) {
    var listFull = `
<li 
  data-title="${book.title}" 
  data-author="${book.author.name}" 
  data-info="${book.info}" 
  data-price="${book.price}">
  <img 
    src="${book.cover.small}">
</li>`;
    return list + listFull;
  }, ``);
    ul.innerHTML = booksList;
  }
}
