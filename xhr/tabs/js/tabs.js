const buttons = document.querySelectorAll('nav a');
const buttonEmail = buttons[0];
const buttonSMS = buttons[1];
const content = document.querySelector('#content');
const loader = document.querySelector('#preloader');

const nav = document.querySelector('nav');

document.addEventListener('DOMContentLoaded', () => {
  buttonEmail.click();
});
nav.addEventListener('click', onClick);

function onClick(e) {
  e.preventDefault();

  if( !e.target.classList.contains('active') ) {
    buttonEmail.classList.toggle('active');
    buttonSMS.classList.toggle('active');
  }
      
  var xhr = new XMLHttpRequest();
      
  xhr.addEventListener("loadstart", () => {
    loader.classList.remove('hidden');
  });
      
  xhr.addEventListener("loadend", () => {
    if (xhr.status == 200) {
      content.innerHTML = xhr.responseText;
    }
  });

  xhr.addEventListener("loadend", () => {
    loader.classList.add('hidden');
  });

  xhr.open("GET", e.target.href);
  xhr.send();
}