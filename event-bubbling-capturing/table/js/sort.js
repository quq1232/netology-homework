'use strict';

function handleTableClick(event) {
  if (event.target.tagName == "TH") {

    if (this.dataset.sortBy == event.target.dataset.propName) {
      event.target.dataset.dir = event.target.dataset.dir * (-1);
    } else {
      event.target.dataset.dir = 1;
    }

    this.dataset.sortBy = event.target.dataset.propName;
    sortTable(event.target.dataset.propName, event.target.dataset.dir);
  }
}
