'use strict';

function handleClick(event) {
  event.preventDefault();
  addToCart({
    title: event.target.dataset.title,
    price: event.target.dataset.price,
  });
}

function reinit() {
  Array
    .from(document.querySelectorAll('.add-to-cart'))
    .forEach((node) => node.addEventListener('click', handleClick));
}

reinit();
showMore.addEventListener('click', reinit);