'use strict';

const counter = document.querySelector('.counter');
const errors = document.querySelector('.errors');
const wss = new WebSocket('wss://neto-api.herokuapp.com/counter');

wss.addEventListener('message', event => {
  const data = JSON.parse(event.data);
  counter.innerHTML = data.connections;
  errors.innerHTML = data.errors;
});


window.addEventListener('beforeunload', () => {
  wss.close(1000);
});