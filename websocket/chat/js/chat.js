'use strict'

const submit = document.querySelector('.message-submit');
const chat = document.querySelector('.chat-status');
const status = document.querySelector('.message-status');
const content = document.querySelector('.messages-content');
const loading = document.querySelector('.loading');
const message = document.querySelectorAll('.message')[1];
const messageP = document.querySelector('.message-personal');
const input = document.querySelector('.message-input');
const box = document.querySelector('.message-box');

box.action="#";
box.method="";

const wss = new WebSocket('wss://neto-api.herokuapp.com/chat');

function sendMsg() {
  wss.send(input.value);
  
  const messagePClon = messageP.cloneNode(true);
  const messagePClonText = messagePClon.querySelector('.message-text');
  const messagePClonTime = messagePClon.querySelector('.timestamp');
  messagePClonText.innerHTML = input.value;
  var date = new Date();
  messagePClonTime.innerHTML = ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);
  content.appendChild(messagePClon);

  input.value = "";
}


function handleKeyUp(event) {
  if ( event.key=="Enter") {
    sendMsg();
  }
}


submit.addEventListener('click', sendMsg);
box.addEventListener('keyup', handleKeyUp);

wss.addEventListener('open', event => {
  chat.innerHTML = chat.dataset.online;
  submit.disabled = false;

  const statusCloneOn = status.cloneNode(true);
  const statusCloneOnText = statusCloneOn.querySelector('.message-text');
  statusCloneOnText.innerHTML = "Пользователь появился в сети";
  content.appendChild(statusCloneOn);
});


wss.addEventListener('message', event => {
  var data = event.data;
  if(data == "...") {
    content.appendChild(loading);
  } else {
    content.removeChild(loading);

    const messageClon = message.cloneNode(true);
    const messageClonText = messageClon.querySelector('.message-text');
    const messageClonTime = messageClon.querySelector('.timestamp');
    messageClonText.innerHTML = data;
    var date = new Date();
    messageClonTime.innerHTML = ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);
    content.appendChild(messageClon);
  }
});


window.addEventListener('beforeunload', () => {
  wss.onclose = () => {
    chat.innerHTML = chat.dataset.offline;
    submit.disabled = true;
    
    status.innerHTML = "Пользователь не в сети";
    const statusCloneOff = status.cloneNode(true);
    const statusCloneOffText = statusCloneOff.querySelector('.message-text');
    statusCloneOffText.innerHTML = "Пользователь не в сети";
    content.appendChild(statusCloneOff);
  }
  wss.close(1000);
});
