'use strinct';

const wss = new WebSocket('wss://neto-api.herokuapp.com/mouse');

wss.addEventListener('open', () => {
  showBubbles(wss);
});

window.addEventListener('click', (event) => {
  if (!wss || !(wss instanceof WebSocket)) {
    return;
  }

  wss.send(JSON.stringify({
    y: event.clientY, 
    x: event.clientX,
  }));
});

window.addEventListener('beforeunload', () => {
  wss.close();
});